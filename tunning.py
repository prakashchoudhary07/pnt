import libvirt 
import subprocess
conn = libvirt.open('qemu:///system')
vm = conn.lookupByID(5)


def vCPUInfo():
	''' This gathers info about vCPU requirements of currently running domains by querying libvirt and counting active vcpus. '''

	runningDomsIDs = conn.listDomainsID()

	domvCPUs = {}
	
	for runningDomID in runningDomsIDs:
		runningDom = conn.lookupByID(runningDomID)
		if runningDom.isActive():
			vmCPUInfo = runningDom.vcpus()[0] # something like [(0, 1, 405400000000L, 5), (1, 1, 142000000000L, 13), (2, 1, 208550000000L, 7), (3, 1, 111900000000L, 15)]
			runningDomvCPUs = 0
			for vCPU in vmCPUInfo:
				if vCPU[1] == 1: runningDomvCPUs += 1 # only count actually running vCPUs

			#print "Domain %d has %d vCPUs" % (runningDomID, runningDomvCPUs)
			domvCPUs[runningDomID] = runningDomvCPUs # add to collection of domains with their vCPUs

	# get a list of (domain ID, domain vCPUs) pairs sorted by descending number of vCPUs
	domsSortedbyvCPUs = sorted(domvCPUs.items())
        print(domsSortedbyvCPUs)
	return domsSortedbyvCPUs # Returning the vCPU info
	''' We now have a structure that holds vcpu allocation requests for currently running domains.
	It is a sorted list of tuples (runningDomID, number of vcpus). e.g.: [(7, 4), (8, 2), (9, 2), (10, 1)].	'''

        

def pCPUInfo():
	''' This gathers info about current configuration of local sockets/cpus/cores/threads.
	Doing this via libvirt yields inconsistent results, so we do it by examining the output of lscpu.
	Different implementations of lscpu might require additional configuration for successful detection of the values. '''

	lscpu = subprocess.check_output(['lscpu'])

	for line in lscpu.split("\n"):
		if line.startswith('CPU(s):'):
			cpus = int(line.split(":")[1].strip())
		elif 'Thread(s) per core:' in line:
			threadsPerCore = int(line.split(":")[1].strip())
		elif 'Core(s) per socket:' in line:
			coresPerSocket = int(line.split(":")[1].strip())
		elif ('Socket(s):' in line) or ('CPU socket(s):' in line):
			sockets = int(line.split(":")[1].strip())
	
	if not (cpus * threadsPerCore * coresPerSocket * sockets):
		print("Error")

	print("%d cpus, %d threadsPerCore, %d coresPerSocket, %d sockets" % (cpus, threadsPerCore, coresPerSocket, sockets))

pCPUInfo()
vCPUInfo()

###################################################CPU PINING STARTS HERE #################################################

map = conn.getCPUMap()
cpumap = str(map[1])
vmRequirement = vCPUInfo()
vmId = vmRequirement[0][0]
vmCpuRequired = vmRequirement[0][1]
print (vmId vmCpuRequired) 
print(cpumap)


for i in range(0,vmCpuRequired):
	(False,True,False,False,False,False,False,False
	vm.pinVcpu(i,())


vm.pinVcpu(0,(True,False,False,False,False,False,False,False))
vm.pinVcpu(1,(True,False,False,False,False,False,False,False))
vm.pinVcpu(2,(True,False,False,False,False,False,False,False))
vm.pinVcpu(3,(True,False,False,False,False,False,False,False))
